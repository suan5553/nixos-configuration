{ config, lib, pkgs, ... }:

let
  cfg = config.services.arcan;
in with lib; {
  options.services.arcan = {
    enable = mkEnableOption "Enable Arcan Display Server";
    enableSuidWrapper = mkOption {
      type = types.bool;
      default = true;
      description = "Enable SUID wrapper, allows Arcan to control backlight among other things";
    };
    package = mkOption {
      type = types.package;
      default = pkgs.arcanPackages.arcan;
      example = pkgs.arcanPackages.arcan;
      description = ''
        Arcan package to use.
      '';
    };
  };

  config = mkIf cfg.enable {
    environment.systemPackages = [
      cfg.package
    ];

    security.wrappers = mkIf cfg.enableSuidWrapper {
      arcan = {
        owner = "root";
        group = "root";
        setuid = true;
        setgid = true;
        source = "${cfg.package}/bin/arcan";
      };
    };
    # Other configuration options go here
  };
}
