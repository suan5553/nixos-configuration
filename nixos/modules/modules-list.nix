{...}:
{
  arcan =  import ./services/arcan.nix;
  nixosConfigurations.default = import ./configurations/default;
  hardware-impure = import ./impure/hardware-configuration.nix;
}
