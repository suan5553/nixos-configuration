{
  description = "A very basic flake";
  inputs = {
    nixpkgs = {
      type = "github";
      owner = "NixOS";
      repo = "nixpkgs";
      ref = "nixos-unstable";
    };
    homeManager = {
      type = "github";
      owner = "nix-community";
      repo = "home-manager";
    };
    flake-compat = {
	type = "github";
	owner = "edolstra";
	repo = "flake-compat";
	flake = false;
};
  };

  outputs = { self, nixpkgs, ... }@exargs:
    let
      system = "x86_64-linux";
      pkgs = import nixpkgs {
        inherit system;
        config = { allowUnfree = true; };
      };
    in {
      nixosConfigurations = with self.outputs.nixosModules; {
        nixos = nixpkgs.lib.nixosSystem {
          inherit system;
          modules = [
        hardware-impure
        nixosConfigurations.default    
        
          
          ];
        };

        };
      nixosModules = import nixos/modules/modules-list.nix {};
        legacyPackages.${system} = pkgs;

      };

    }


